# Rixos Nuxt app

Rixos site catalog, powered by Laravel 5.6 & Nuxt 1.4.

### Installation

This project requires [Node.js](https://nodejs.org/) v4+ to run.

```sh
$ cd rixos-ssr-spa
```
1. Cofigure and edit env, set db, and api,admin,front domains

2. Install the dependencies and devDependencies and start the server.

```sh
$ composer install
$ php artisan migrate --seed
$ cd client   
$ npm install
$ npm run dev
```

3. Open http://localhost:3000, Enjoy!

### Todos

 - Show message if no products found on catalog list
 - Feedback API 
 - Animate BXSlider

License
----

MIT


**Free Software, Hell Yeah!**
