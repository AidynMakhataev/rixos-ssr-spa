<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
   return ['hello from api'];
});

Route::get('/products', 'ProductAPIController@index');
Route::get('/partials/sliders', 'PartialsAPIController@getSliders');
Route::get('/partials/partners', 'PartialsAPIController@getPartners');
Route::get('/partials/certs', 'PartialsAPIController@getCerts');

Route::get('/categories', 'CategoryAPIController@index');
Route::get('/categories/featured', 'CategoryAPIController@getFeaturedCategories');
Route::get('/categories/{category}', 'CategoryAPIController@getCategoryBySlug');
Route::get('/categories/{category}/products', 'CategoryAPIController@getCategoryProducts');
Route::get('/categories/{category}/{subCategory}/products', 'CategoryAPIController@getSubCategoryProducts');
Route::get('/categories/{category}/products/{product}', 'CategoryAPIController@getProductByCategoryAndProductSlug');
