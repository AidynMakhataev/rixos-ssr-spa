<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {


    return [
        'name'          =>  'RIXOS '. $faker->text(6),
        'status'        =>  1,
        'category_id'   =>  $faker->numberBetween(1,23),
        'image'         =>  'dummy/products/'.$faker->numberBetween(1,15).'.png',
        'extras'        =>  json_encode([
            'characteristics' => [
                [
                    'name'  =>  $faker->text(8),
                    'desc'  =>  $faker->sentence(4)
                ],
                [
                    'name'  =>  $faker->text(8),
                    'desc'  =>  $faker->sentence(4)
                ],
                [
                    'name'  =>  $faker->text(8),
                    'desc'  =>  $faker->sentence(4)
                ]
            ],
            'excerpt'   =>  $faker->sentence(4)
        ])
    ];
});
