<?php

use Illuminate\Database\Seeder;
use App\Models\Certification;

class CertificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 4; $i ++)
        {
            Certification::create([
                'name'  =>  'Certificate '. ($i+1),
                'image' =>  'dummy/certifications/cert1.png'
            ]);
        }
    }
}
