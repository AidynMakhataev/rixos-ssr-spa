<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Средства для мытья посуды', 'Жидкое мыло', 'Бальзам кондиционер для белья',
            'Стеклоочеститель', 'Освежитель воздуха', 'Шапунь для волос',
            'Моющие и чистящие средства', 'Гель универсальный, дезинфецирующий',
            'Моющие и чистящие средства', 'Гель универсальный, дезинфецирующий',
            'Средство для подгоревшего жира', 'Средство для чистки ковров и шерсти',
            'Средство для автомашин', 'Средство для бесконтактной мойки',
            'Салфетки и туалетная бумага', 'Ватные диски и ушные палочки', 'Мыло Farissa',
            'Мыло Clonex', 'Мыло Rixos', 'Мыло Elsa & Maryna'
        ];

        foreach($categories as $category) {
            $cat = \App\Models\Category::create(['name' => $category]);
            // factory(App\Models\Product::class, 15)->create([
            //     'category_id'   =>  $cat->id
            // ]);
        }

        $categoriesWithChild = [
            [
                'name' => 'Стиральные порошки',
                'child' => [
                    'Для цветного белья', 'Для белого белья'
                ]
            ]
        ];

        foreach($categoriesWithChild as $category) {
            $parent = \App\Models\Category::create(['name' => $category['name']]);

            // factory(App\Models\Product::class, 15)->create([
            //     'category_id'   =>  $parent->id
            // ]);
            foreach($category['child'] as $item) {
                $cat = \App\Models\Category::create([
                    'name'      => $item,
                    'parent_id' =>  $parent->id
                ]);

                // factory(App\Models\Product::class, 15)->create([
                //     'category_id'   =>  $cat->id
                // ]);
            }
        }
    }
}
