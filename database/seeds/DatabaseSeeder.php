<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'      =>  'Super Admin',
            'email'     =>  'admin@admin.com',
            'password'  =>  bcrypt('password')
        ]);


        $this->call(CategoryTableSeeder::class);
        $this->call(SliderTableSeeder::class);
        $this->call(PartnerTableSeeder::class);
        $this->call(CertificationTableSeeder::class);

        DB::unprepared(file_get_contents(base_path().'/products.sql'));
    }
}
