<?php

use Illuminate\Database\Seeder;
use App\Models\Slider;

class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 5; $i ++) {
            Slider::create([
                'title'     =>  'НОВИНКА <br>НОВЫЙ ШАМПУНЬ <br>ДЛЯ ВОЛОС',
                'image'     =>  'dummy/sliders/ban1.png',
                'status'    =>  1
            ]);
        }
    }
}
