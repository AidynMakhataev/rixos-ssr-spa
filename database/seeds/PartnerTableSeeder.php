<?php

use Illuminate\Database\Seeder;
use App\Models\Partner;

class PartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 6; $i++) {
            Partner::create([
                'name'      =>  'Partner '.($i + 1),
                'logo'      =>  'dummy/partners/partner' . ($i + 1) . '.png',
                'status'    =>  1
            ]);
        }
    }
}
