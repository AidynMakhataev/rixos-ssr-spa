<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Partner;
use App\Models\Certification;
use App\Http\Resources\SliderResource;
use App\Http\Resources\PartnerResource;
use App\Http\Resources\CertificationResource;



class PartialsAPIController extends Controller
{
    public function getSliders()
    {
        $sliders = Slider::published()->orderBy('lft')->get();

        return response()->json([
            'data'      =>  SliderResource::collection($sliders),
            'message'   =>  'Sliders successfully retrieved.'
        ]);
    }

    public function getPartners()
    {
        $partners = Partner::published()->orderBy('lft')->get();

        return response()->json([
            'data'      =>  PartnerResource::collection($partners),
            'message'   =>  'Partners successfully retrieved.'
        ]);
    }

    public function getCerts()
    {
        $certs = Certification::orderBy('lft')->get();

        return response()->json([
            'data'      =>  CertificationResource::collection($certs),
            'message'   =>  'Certifications successfully retrieved.'
        ]);
    }
}
