<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CertificationRequest as StoreRequest;
use App\Http\Requests\CertificationRequest as UpdateRequest;

class CertificationCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Certification');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/certification');
        $this->crud->setEntityNameStrings('сертификат', 'сертификаты');

        $this->crud->enableReorder('name', 1);
        $this->crud->allowAccess('reorder');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Название'
        ]);
        $this->crud->addColumn([
            'name'      =>  'image',
            'label'     =>  'Изображение',
            'type'      =>  'image',
            'prefix'    =>  config('app.mode') == 'production' ? 'uploads/' : '',
            'height'    =>  '100px'
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Название',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'name' => 'image',
            'label' => 'Изображение',
            'type' => 'image',
            'upload' => true,
            'prefix' => config('app.mode') == 'production' ? 'uploads/' : ''
        ]);

        
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function saveReorder()
    {
        $this->crud->hasAccessOrFail('reorder');

        $all_entries = \Request::input('tree');

        if (count($all_entries)) {
            $count = 0;

            foreach ($all_entries as $key => $entry) {
                if ($entry['item_id'] != '' && $entry['item_id'] != null) {
                    $item = $this->crud->model->find($entry['item_id']);
                    $item->lft = empty($entry['left']) ? null : $entry['left'];
                    $item->save();

                    $count++;
                }
            }
        } else {
            return false;
        }

        return 'success for '.$count.' items';
    }
}
