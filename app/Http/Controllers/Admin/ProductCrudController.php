<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductRequest as StoreRequest;
use App\Http\Requests\ProductRequest as UpdateRequest;

class ProductCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('товар', 'товары');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn(
            [
                'name' => 'name',
                'label' => 'Наименование товара',
                'type'  =>  'text'
            ]
        );

        $this->crud->addColumn(
            [
                'label' => 'Категория',
                'type' => 'select',
                'name' => 'category_id',
                'entity' => 'category',
                'attribute' => 'name',
                'model' => "App\Models\Category",
            ]
        );

        $this->crud->addColumn(
            [
                'name' => 'status',
                'label' => 'В наличии',
                'type'  =>  'check'
            ]
        );


        $this->crud->addField(
            [
                'name' => 'name',
                'label' => 'Наименование товара',
                'type'  =>  'text'
            ]
        );

        $this->crud->addField(
            [
                'name' => 'slug',
                'label' => 'Slug (URL)',
                'type' => 'text',
                'hint' => 'Сгенерируется автоматически с заголовка, если оставить это поле пустым!',
                // 'disabled' => 'disabled'
            ]
        );

        $this->crud->addField(
            [
                'label' => 'Категория',
                'type' => 'select',
                'name' => 'category_id',
                'entity' => 'category',
                'attribute' => 'name',
                'model' => "App\Models\Category",
            ]
        );

        $this->crud->addField(
            [
                'label'     =>  'Изображение товара',
                'type'      =>  'image',
                'name'      =>  'image',
                'upload'    =>   true,
                'prefix'    =>   config('app.mode') == 'production' ? 'uploads/' : '',
            ]
        );

        $this->crud->addField(
            [
                'name'      =>  'excerpt',
                'label'     =>  'Краткое описание товара',
                'type'      =>  'textarea',
                'fake'      =>  true,
                'store_in'  =>  'extras'
            ]
        );

        $this->crud->addField(
            [
                'name'  =>  'characteristics',
                'label' => 'Характеристики',
                'type' => 'table',
                'entity_singular' => 'пункт', // used on the "Add X" button
                'columns' => [
                    'name' => 'Заголовок',
                    'desc' => 'Описание'
                ],
                'max' => 12, // maximum rows allowed in the table
                'min' => 0, // minimum rows allowed in the table,
                'fake'  =>  true,
                'store_in'  =>  'extras'
            ]
        );

        $this->crud->addField(
            [
                'name'      =>  'appointment',
                'label'     =>  'Назначение',
                'type'      =>  'textarea',
                'fake'      =>  true,
                'store_in'  =>  'extras'
            ]
        );

        $this->crud->addField(
            [
                'name'      =>  'application_area',
                'label'     =>  'Область применения',
                'type'      =>  'textarea',
                'fake'      =>  true,
                'store_in'  =>  'extras'
            ]
        );

        $this->crud->addField(
            [
                'name'      =>  'consumer_properties',
                'label'     =>  'Потребительские свойства',
                'type'      =>  'table',
                'fake'      =>  true,
                'entity_singular' => 'свойство', // used on the "Add X" button
                'columns' => [
                    'name' => 'Свойство',
                ],
                'max' => 12, // maximum rows allowed in the table
                'min' => 0, // minimum rows allowed in the table,
                'store_in'  =>  'extras'
            ]
        );

        $this->crud->addField(
            [
                'name'      =>  'recommendations',
                'label'     =>  'Рекомендации по применению',
                'type'      =>  'table',
                'entity_singular' => 'рекомендацию', // used on the "Add X" button
                'columns' => [
                    'name' => 'Рекомендация',
                ],
                'max' => 12, // maximum rows allowed in the table
                'min' => 0, // minimum rows allowed in the table,
                'fake'      =>  true,
                'store_in'  =>  'extras'
            ]
        );

        $this->crud->addField(
            [
                'name' => 'status',
                'label' => 'В наличии',
                'type'  =>  'checkbox'
            ]
        );
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
