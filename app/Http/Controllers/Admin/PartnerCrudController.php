<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PartnerRequest as StoreRequest;
use App\Http\Requests\PartnerRequest as UpdateRequest;

class PartnerCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Partner');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/partner');
        $this->crud->setEntityNameStrings('партнер', 'партнеры');

        $this->crud->enableReorder('name', 1);
        $this->crud->allowAccess('reorder');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Название компании'
        ]);
        $this->crud->addColumn([
            'name' => 'logo',
            'label' => 'Логотип',
            'type' => 'image',
            'prefix' => config('app.mode') == 'production' ? 'uploads/' : '',
            'height' => '100px'
        ]);
        
        $this->crud->addField([
            'name' => 'name',
            'label' => 'Название компании',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'url',
            'label' => 'Ссылка на сайт компании',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'logo',
            'label' => 'Логотип',
            'type' => 'image',
            'upload' => true,
            'prefix' => config('app.mode') == 'production' ? 'uploads/' : ''
        ]);
        $this->crud->addField(
            [
                'name' => 'status',
                'label' => 'Опубликован',
                'type'  =>  'checkbox'
            ]
        );
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function saveReorder()
    {
        $this->crud->hasAccessOrFail('reorder');

        $all_entries = \Request::input('tree');

        if (count($all_entries)) {
            $count = 0;

            foreach ($all_entries as $key => $entry) {
                if ($entry['item_id'] != '' && $entry['item_id'] != null) {
                    $item = $this->crud->model->find($entry['item_id']);
                    $item->lft = empty($entry['left']) ? null : $entry['left'];
                    $item->save();

                    $count++;
                }
            }
        } else {
            return false;
        }

        return 'success for '.$count.' items';
    }
}
