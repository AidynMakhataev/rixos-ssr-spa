<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoryRequest as StoreRequest;
use App\Http\Requests\CategoryRequest as UpdateRequest;

class CategoryCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('категория', 'категории');

        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('name', 2);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn(
            [
                'name' => 'name',
                'label' => 'Заголовок',
                'type'  =>  'text'
            ]
        );

        $this->crud->addColumn(
            [
                'label' => 'Родитель',
                'type' => 'select',
                'name' => 'parent_id',
                'entity' => 'parent',
                'attribute' => 'name',
                'model' => "App\Models\Category",
            ]
        );

        $this->crud->addField(
            [
                'name' => 'name',
                'label' => 'Заголовок',
                'type'  =>  'text'
            ]
        );
        $this->crud->addField(
            [
                'label' => 'Родитель',
                'type' => 'select',
                'name' => 'parent_id',
                'entity' => 'parent',
                'attribute' => 'name',
                'model' => "App\Models\Category",
            ]
        );
        $this->crud->addField(
            [
                'label' =>  'Избранное',
                'type'  =>  'checkbox',
                'name'  =>  'featured'
            ]
        );







    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
