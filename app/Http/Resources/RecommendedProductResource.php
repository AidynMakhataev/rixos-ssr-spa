<?php

namespace App\Http\Resources;

class RecommendedProductResource extends ProductResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    =>  $this->id,
            'name'  =>  $this->name,
            'slug'  =>  $this->slug,
            'image' =>  $this->getImage(),
            'url'   =>  $this->getUrl()
        ];
    }
}
