<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'      =>  $this->name,
            'products'  =>  $this->when($request->get('include') == 'products', ProductResource::collection($this->products()->take(3)->get())),
            'slug'      =>  $this->slug
        ];
    }
}
