<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $withRequest = $request->get('with');

        return [
            'id'                    =>  $this->id,
            'name'                  =>  $this->name,
            'url'                   =>  $this->getUrl(),
            'slug'                  =>  $this->slug,
            'catSlug'               =>  $this->category->slug,
            'status'                =>  $this->status == 1 ? 'В наличии' : 'Нет в наличии',
            'excerpt'               =>  $this->withFakes()->excerpt,
            'characteristics'       =>  $this->when($this->requestHasAttribute($withRequest, 'characteristics'), json_decode($this->withFakes()->characteristics)),
            'apointment'            =>  $this->when($this->requestHasAttribute($withRequest, 'appointment'), strip_tags($this->withFakes()->appointment)),
            'application_area'      =>  $this->when($this->requestHasAttribute($withRequest, 'application_area'), strip_tags($this->withFakes()->application_area)),
            'consumer_properties'   =>  $this->when($this->requestHasAttribute($withRequest, 'consumer_properties'), json_decode($this->withFakes()->consumer_properties)),
            'recommendations'       =>  $this->when($this->requestHasAttribute($withRequest, 'recommendations'), $this->withFakes()->recommendations),
            'recommended_products'  =>  $this->when($this->requestHasAttribute($withRequest, 'recommended_products'), $this->getRecommendedProducts()),
            'image'                 =>  $this->getImage()
        ];
    }

    protected function requestHasAttribute($request, $attribute)
    {
       $attributes = explode(',', $request);
       
        return in_array($attribute, $attributes);
    }

    protected function getUrl()
    {
        return isset($this->category->parent) ? '/catalog/'. $this->category->parent->slug . '/' . $this->slug : '/catalog/' . $this->category->slug . '/' . $this->slug;
    }

    protected function getRecommendedProducts()
    {
        $products = $this->category->products()->where('id', '!=', $this->id)->take(7)->get();
        
        return RecommendedProductResource::collection($products);
    }

    protected function getImage()
    {
        if(isset($this->image)) {
            switch ($this->image) {
                case starts_with($this->image, 'dummy'):
                    return asset($this->image);
                    break;    
                default:
                    return asset('uploads/'.$this->image); 
                    break;
            }
        } else {
            return asset('dummy/products/not_available.jpg');
        }
    }
}
