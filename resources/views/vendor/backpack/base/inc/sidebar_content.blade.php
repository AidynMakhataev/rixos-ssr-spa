<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/category') }}'><i class='fa fa-list'></i> <span>Категории</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/product') }}'><i class='fa fa-product-hunt'></i> <span>Товары</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/slider') }}'><i class='fa fa-file-image-o'></i> <span>Слайдер</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/partner') }}'><i class='fa fa-handshake-o'></i> <span>Партнеры</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/certification') }}'><i class='fa fa-file'></i> <span>Сертификаты</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/backup') }}'><i class='fa fa-hdd-o'></i> <span>Бэкапы</span></a></li>