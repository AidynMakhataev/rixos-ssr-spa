import Vuex from 'vuex'
import axios from '@/plugins/axios'


const createStore = () => {
  return new Vuex.Store({
    state: {
        categories: [],
        partners: [],
        certs: []
    },
    getters: {
        loadedCategories(state) {
            return state.categories
        },
        loadedPartners(state) {
            return state.partners        
        },
        loadedCerts(state) {
            return state.certs
        }
    },
    mutations: {
        setCategories(state, categories) {
            state.categories = categories
        },
        setPartners(state, partners) {
            state.partners = partners
        },
        setCerts(state, certs) {
            state.certs = certs
        }
    },
    actions: {
        nuxtServerInit (context) {
            return Promise.all([    
                context.dispatch('getCategories'),
                context.dispatch('getPartners'),
                context.dispatch('getCerts')
            ])
        },
        getCategories({commit}) {
            return  axios.get('/categories')
            .then((response) => {
                commit('setCategories', response.data.data)
            })
        },
        getPartners({commit}) {
            return axios.get('/partials/partners')
                .then((response) => {
                    commit('setPartners', response.data.data)
                })
        },
        getCerts({commit}) {
            return axios.get('/partials/certs')
                .then((response) => {
                    commit('setCerts', response.data.data)
                })
        }
    }
  })
}

export default createStore
